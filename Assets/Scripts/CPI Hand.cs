using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CPIHand : MonoBehaviour
{
    [SerializeField]
    private float movingRate = 500f, moveSpeed = 5f;
    [SerializeField]
    Image imgHand, imgTap;
    [SerializeField]
    Sprite handDown, handUp;

    Vector3 lastMousePosition;
    //bool canMove = false;
    Color transparentColor;

    private void Awake()
    {
        transparentColor = Color.white;
        transparentColor.a = 0;
    }

    private void OnEnable()
    {
        imgHand.DOFade(1f, 0.125f);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            float duration = Vector2.Distance(imgHand.transform.position, Input.mousePosition) / movingRate;

            //imgHand.transform.DOKill();
            //imgHand.transform.DOMove(Input.mousePosition, duration).OnComplete(() =>
            //{
                ShowTapAnimation();
            //});
        }

        if (Input.GetMouseButtonUp(0))
        {
            ShowUntapAnimation();
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            imgTap.gameObject.SetActive(false);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            imgTap.gameObject.SetActive(true);
        }

        //if (canMove)
        //{
        Vector3 currentMousePosition = Input.mousePosition;
            Vector3 deltaPosition = currentMousePosition - lastMousePosition;
            Vector3 newPos = imgHand.transform.position + deltaPosition.normalized;

            imgHand.transform.position = Vector2.Lerp(imgHand.transform.position, currentMousePosition, moveSpeed * Time.deltaTime);
            lastMousePosition = currentMousePosition;
        //}

        imgTap.GetComponent<RectTransform>().position = imgHand.GetComponent<RectTransform>().position;
    }

    Sequence sequence;

    void ShowTapAnimation()
    {
        sequence.Kill();
        sequence = DOTween.Sequence();
        imgTap.transform.DOScale(Vector3.one, 0.2f);
        sequence.Append(imgTap.DOFade(1, 0.1f));
        sequence.AppendInterval(0.15f);
        sequence.Append(imgTap.DOFade(0, 0.15f));
        sequence.Append(imgTap.transform.DOScale(Vector3.zero, 0.25f));

        imgHand.sprite = handDown;
    }

    void ShowUntapAnimation()
    {
        Invoke("HandUp", 0.25f);
    }

    void HandUp()
    {
        imgHand.sprite = handUp;
    }
}
