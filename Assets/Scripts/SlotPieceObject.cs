using System;
using System.Collections.Generic;
using UnityEngine;

public class SlotPieceObject : MonoBehaviour
{
    public SlotPiece slotPiece;
    // public PuzzlePieceObject puzzlePieceObject;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
    public Color color;
    [Range(0f, 1f)]
    public float transfarency = 0.7f;
    
    public void SetMesh(Mesh mesh)
    {
        meshFilter.mesh = mesh;
    }

    public void SetMaterial(Material[] materials)
    {
        meshRenderer.materials = materials;
        meshRenderer.enabled = true;
    }

    public void Reset()
    {
        slotPiece.isEmpty = true;
        slotPiece.puzzlePiece.color = ShapeColor.NoColor;
        meshRenderer.materials = LevelManager.Singleton.GetMaterial(slotPiece.puzzlePiece.color, slotPiece.puzzlePiece.materialCount);
        if(slotPiece.puzzlePiece.size == ShapeSize.Large) return;
        meshRenderer.enabled = false;
    }
}
