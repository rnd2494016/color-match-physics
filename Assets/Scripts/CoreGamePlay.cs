using System;
using System.Collections.Generic;
using System.Linq;
using Sakhawat.Singleton;
using UnityEngine;

public class CoreGamePlay : SingletonTemplate<CoreGamePlay>
{
    public int slotDepth = 3;
    public LevelInfo currentLevel;
    
    [SerializeField]private TextAsset levelDataJson;
    public AllLevelData allLevelData;
    
    public override void Awake()
    {
        base.Awake();
        allLevelData = JsonUtility.FromJson<AllLevelData>(levelDataJson.text);
        // Debug.Log("Data loaded from json");
    }

    private void Start()
    {
        currentLevel = GetCurrentLevelData(1);
        LevelManager.Singleton.LevelGenerate();
        // Invoke(nameof(StartLevel), 0.5f);
    }

    private void StartLevel()
    {
        LevelManager.Singleton.LevelGenerate();
    }

    public LevelInfo GetCurrentLevelData(int levelNumber)
    {
        LevelInfo levelInfo = new LevelInfo
        {
            levelNumber= allLevelData.levels[levelNumber - 1].level_number,
            puzzlePieces = new List<PuzzlePiece>(),
            slots = new List<Slot>()
        };
        foreach (SinglePieceData singlePieceData in allLevelData.levels[levelNumber - 1].puzzle_piece)
        {
            Enum.TryParse(singlePieceData.shape, out Shape shapeData);
            for (int i = 0; i < singlePieceData.puzzle_piece_data.Count; i++)
            {
                for (int j = 0; j < singlePieceData.puzzle_piece_data[i].set; j++)
                {
                    PuzzlePiece puzzlePieceLarge = new PuzzlePiece
                    {
                        shape = shapeData,
                        size = ShapeSize.Large,
                        color = ShapeColor.NoColor,
                        materialCount = 2
                    };
                    Enum.TryParse(singlePieceData.puzzle_piece_data[i].color, out puzzlePieceLarge.color);
                    levelInfo.puzzlePieces.Add(puzzlePieceLarge);
                    PuzzlePiece puzzlePieceMedium = new PuzzlePiece
                    {
                        shape = shapeData,
                        size = ShapeSize.Medium,
                        color = ShapeColor.NoColor,
                        materialCount = 1
                    };
                    Enum.TryParse(singlePieceData.puzzle_piece_data[i].color, out puzzlePieceMedium.color);
                    levelInfo.puzzlePieces.Add(puzzlePieceMedium);
                    PuzzlePiece puzzlePieceSmall = new PuzzlePiece
                    {
                        shape = shapeData,
                        size = ShapeSize.Small,
                        color = ShapeColor.NoColor,
                        materialCount = 1
                    };
                    Enum.TryParse(singlePieceData.puzzle_piece_data[i].color, out puzzlePieceSmall.color);
                    levelInfo.puzzlePieces.Add(puzzlePieceSmall);
                }
            }
        }
        foreach (SinglePieceData singlePieceData in allLevelData.levels[levelNumber - 1].slot_piece)
        {
            Enum.TryParse(singlePieceData.shape, out Shape shapeData);
            for (int i = 0; i < singlePieceData.puzzle_piece_data.Count; i++)
            {
                for (int j = 0; j < singlePieceData.puzzle_piece_data[i].set; j++)
                {
                    List<SlotPiece> slotPieces = new List<SlotPiece>();
                    SlotPiece slotPieceLarge = new SlotPiece
                    {
                        puzzlePiece = default,
                        isEmpty = true
                    };
                    slotPieceLarge.puzzlePiece.shape = shapeData;
                    slotPieceLarge.puzzlePiece.size = ShapeSize.Large;
                    slotPieceLarge.puzzlePiece.materialCount = 2;
                    slotPieces.Add(slotPieceLarge);
                    SlotPiece slotPieceMedium = new SlotPiece
                    {
                        puzzlePiece = default,
                        isEmpty = true
                    };
                    slotPieceMedium.puzzlePiece.shape = shapeData;
                    slotPieceMedium.puzzlePiece.size = ShapeSize.Medium;
                    slotPieceMedium.puzzlePiece.materialCount = 1;
                    slotPieces.Add(slotPieceMedium);
                    SlotPiece slotPieceSmall = new SlotPiece
                    {
                        puzzlePiece = default,
                        isEmpty = true
                    };
                    slotPieceSmall.puzzlePiece.shape = shapeData;
                    slotPieceSmall.puzzlePiece.size = ShapeSize.Small;
                    slotPieceSmall.puzzlePiece.materialCount = 1;
                    slotPieces.Add(slotPieceSmall);
                    Slot slot = new Slot
                    {
                        slotPieces = new List<SlotPiece>(),
                        availableColor = ShapeColor.NoColor,
                        hasMatch = false
                    };
                    slot.slotPieces = slotPieces;
                    levelInfo.slots.Add(slot);
                }
            }
        }

        return levelInfo;
    }
    
    public (Slot, SlotPiece, bool) IsSlotAvailableForPiece(PuzzlePiece puzzlePiece)
    {
        foreach (Slot slot in currentLevel.slots)
        {
            if (slot.availableColor == ShapeColor.NoColor || slot.availableColor == puzzlePiece.color)
            {
                foreach (SlotPiece slotPiece in slot.slotPieces.Where(slotPiece => slotPiece.isEmpty && puzzlePiece.shape == slotPiece.puzzlePiece.shape && puzzlePiece.size == slotPiece.puzzlePiece.size))
                {
                    if (slot.availableColor == ShapeColor.NoColor) 
                        slot.availableColor = puzzlePiece.color;
                    slotPiece.isEmpty = false;
                    slotPiece.puzzlePiece = puzzlePiece;
                    CheckMatch(slot);
                    return (slot, slotPiece, true);
                }
            }
        } 
        return (null, null, false);
    }

    private void CheckMatch(Slot slot)
    {
        int matchCount = 0;
        foreach (SlotPiece slotPiece in slot.slotPieces)
        {
            if (!slotPiece.isEmpty) matchCount++;
        }
        if (matchCount != slotDepth) return;
        slot.hasMatch = true;
    }

    public void ResetSlotContainMatch(Slot slot)
    {
        foreach (SlotPiece slotPiece in slot.slotPieces)
        {
            slotPiece.puzzlePiece.color = ShapeColor.NoColor;
            slotPiece.isEmpty = true;
        }
        slot.availableColor = ShapeColor.NoColor;
        slot.hasMatch = false;
    }
}
