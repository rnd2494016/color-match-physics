using System;
using System.Collections;
using DG.Tweening;
using Sakhawat.System.Generic;
using UnityEngine;

public class PuzzlePieceObject : MonoBehaviour
{
    private bool hasMediumChild, hasSmallChild, hasLargeParent, hasMediumParent;
    public PuzzlePiece puzzlePiece;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;

    private void OnMouseDown()
    {
        (Slot slot, SlotPiece slotPiece, bool isAvailable) = CoreGamePlay.Singleton.IsSlotAvailableForPiece(puzzlePiece);
        if (isAvailable)
        {
            foreach (SlotPieceObject slotPieceObject in LevelManager.Singleton.slotPieceObjects)
            {
                if(!slotPieceObject.slotPiece.isEmpty) continue;
                if (slotPieceObject.slotPiece.puzzlePiece.shape == slotPiece.puzzlePiece.shape && slotPieceObject.slotPiece.puzzlePiece.size == slotPiece.puzzlePiece.size)
                {
                    slotPieceObject.slotPiece.puzzlePiece = puzzlePiece;
                    slotPieceObject.slotPiece.isEmpty = false;
                    CoroutineManager.Singleton.coroutines.Enqueue(MoveToSlot(slot, slotPieceObject));
                    break;
                }
            }
        }
        else
        {
            transform.DOShakeScale(0.1f, 0.1f, 1);
            Transform slotGroup = LevelManager.Singleton.GetSlotGroup(puzzlePiece.shape);
            slotGroup.DOShakeScale(0.1f, 0.1f, 1);
        }
    }

    private IEnumerator MoveToSlot(Slot slot, SlotPieceObject targetSlot)
    {
        yield return null;
        Vector3 mid = (transform.position + targetSlot.transform.position) / 2;
        mid.z -= 5f;
        Vector3[] path = new[] { transform.position, mid, targetSlot.transform.position };
        transform.DOPath(path, 0.3f).SetEase(Ease.Linear).OnComplete(() =>
        {
            gameObject.SetActive(false);
            targetSlot.SetMaterial(meshRenderer.materials);
        });
        transform.DORotate(Vector3.zero, 0.3f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.5f);
        
        if (slot.hasMatch)
            LevelManager.Singleton.SlotMatch(slot);
        LevelManager.Singleton.puzzlePieceObjects.Remove(this);
        Destroy(gameObject);
    }
    
    public void SetMesh(Mesh mesh)
    {
        meshFilter.mesh = mesh;
    }

    public void SetMaterial(Material[] material)
    {
        meshRenderer.materials = material;
    }
    
    public void AddChild(PuzzlePieceObject child)
    {
        if (child.puzzlePiece.size == ShapeSize.Medium)
        {
            if(!hasMediumChild)
            {
                if(puzzlePiece.size == ShapeSize.Large && !child.hasLargeParent)
                {
                    child.transform.position = transform.position;
                    child.hasLargeParent = true;
                    hasMediumChild = true;
                }
            }
        }
        else if (child.puzzlePiece.size == ShapeSize.Small)
        {
            if(!hasSmallChild)
            {
                if(puzzlePiece.size == ShapeSize.Large && !child.hasLargeParent)
                    child.hasLargeParent = true;
                else if (puzzlePiece.size == ShapeSize.Medium && !child.hasMediumChild)
                    child.hasMediumChild = true;
                else return;
                
                child.transform.position = transform.position;
                hasSmallChild = true;
            }
        }
    }

    public void SetParent(PuzzlePieceObject parent)
    {
        if (parent.puzzlePiece.size == ShapeSize.Large)
        {
            if(!hasLargeParent)
            {
                if(puzzlePiece.size == ShapeSize.Medium && !parent.hasMediumChild)
                    parent.hasMediumChild = true;
                else if (puzzlePiece.size == ShapeSize.Small && !parent.hasSmallChild)
                    parent.hasSmallChild = true;
                else return;
                
                parent.transform.position = transform.position;
                hasLargeParent = true;
            }
        }
        else if (parent.puzzlePiece.size == ShapeSize.Medium)
        {
            if(!hasMediumParent)
            {
                if (puzzlePiece.size == ShapeSize.Small && !parent.hasSmallChild)
                    parent.hasSmallChild = true;
                else return;
                
                parent.transform.position = transform.position;
                hasLargeParent = true;
            }
        }
    }
}
