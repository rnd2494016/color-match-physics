using System;
using System.Collections.Generic;
using System.Linq;
using Sakhawat.Singleton;
using Unity.VisualScripting;
using UnityEngine;
using Random = System.Random;

public class LevelManager : SingletonTemplate<LevelManager>
{
    public PuzzlePieceObject puzzlePieceObjectPrefab;
    public SlotPieceObject slotPieceObjectPrefab;
    public Transform puzzlePieceObjectParent;
    [Space]
    public List<PuzzlePieceMesh> allMeshes;
    public List<PieceMaterial> pieceMaterials;
    [Space]
    public List<PuzzlePieceObject> puzzlePieceObjects;
    public List<SlotPieceObject> slotPieceObjects;
    [Space] 
    public List<SlotGroup> slotGroups;

    [Space] 
    public Camera mainCamera;
    public RectTransform canvasRectTransform;

    private void Start()
    {
        SetCameraSize();
    }


    public void LevelGenerate()
    {
        foreach (PuzzlePiece puzzlePiece in CoreGamePlay.Singleton.currentLevel.puzzlePieces)
        {
            PuzzlePieceObject newPieceObject = Instantiate(puzzlePieceObjectPrefab, puzzlePieceObjectParent, true);
            newPieceObject.gameObject.SetActive(false);

            newPieceObject.puzzlePiece = puzzlePiece;
            newPieceObject.SetMesh(GetMesh(puzzlePiece.shape, puzzlePiece.size));
            newPieceObject.gameObject.AddCollider(puzzlePiece.shape);
            newPieceObject.SetMaterial(GetMaterial(puzzlePiece.color, puzzlePiece.materialCount));
            puzzlePieceObjects.Add(newPieceObject);
        }
        Shuffle(puzzlePieceObjects);
    }
    
    private void Shuffle<T>(IList<T> list)
    {
        int n = list.Count;
        Random rng = new Random();
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            (list[k], list[n]) = (list[n], list[k]);
        }
        AppearPuzzleObjects();
    }

    private void AppearPuzzleObjects()
    {
        Vector3 position = Vector3.zero;
        position.x = -3;
        float direction = 1;
        
        foreach (PuzzlePieceObject puzzlePieceObject in puzzlePieceObjects)
        {
            puzzlePieceObject.transform.position = position;
            puzzlePieceObject.gameObject.SetActive(true);
            position.x += 3 * direction;
            if (position.x == 3 || position.x == -3)
            {
                direction *= -1;
                position.y += 3;
            }
        }
        // MakeLayer();
        Invoke(nameof(ActivateFrustumCulling), 5f);
    }

    private void ActivateFrustumCulling()
    {
        foreach (PuzzlePieceObject puzzlePieceObject in puzzlePieceObjects)
        {
            puzzlePieceObject.gameObject.AddFrustumCulling();
        }
    }

    private void MakeLayer()
    {
        List<PuzzlePieceObject> largeObjects = new List<PuzzlePieceObject>();
        List<PuzzlePieceObject> mediumObjects = new List<PuzzlePieceObject>();
        List<PuzzlePieceObject> smallObjects = new List<PuzzlePieceObject>();
        foreach (PuzzlePieceObject puzzlePieceObject in puzzlePieceObjects)
        {
            switch (puzzlePieceObject.puzzlePiece.size)
            {
                case ShapeSize.Large:
                    largeObjects.Add(puzzlePieceObject);
                    break;
                case ShapeSize.Medium:
                    mediumObjects.Add(puzzlePieceObject);
                    break;
                case ShapeSize.Small:
                    break;
                default:
                    smallObjects.Add(puzzlePieceObject);
                    break;
            }
        }
        //AssignParentsAndChildren(largeObjects, mediumObjects, smallObjects);
    }
    
    
    private void AssignParentsAndChildren(List<PuzzlePieceObject> largeObjects, List<PuzzlePieceObject> mediumObjects, List<PuzzlePieceObject> smallObjects)
    {
       Random rand = new Random();

        foreach (PuzzlePieceObject largeObject in largeObjects)
        {
            bool addMediumChild = rand.NextDouble() > 0.5;
            bool addSmallChild = rand.NextDouble() > 0.5;

            if (addMediumChild)
            {
                foreach (PuzzlePieceObject mediumObject in mediumObjects)
                {
                    if (mediumObject.puzzlePiece.shape == largeObject.puzzlePiece.shape && rand.NextDouble() > 0.5)
                    {
                        largeObject.AddChild(mediumObject);
                    }
                }
            }

            if (addSmallChild)
            {
                foreach (PuzzlePieceObject smallObject in smallObjects)
                {
                    if (smallObject.puzzlePiece.shape == largeObject.puzzlePiece.shape && rand.NextDouble() > 0.5)
                    {
                        largeObject.AddChild(smallObject);
                    }
                }
            }
        }

        foreach (PuzzlePieceObject mediumObject in mediumObjects)
        {
            bool addSmallChild = rand.NextDouble() > 0.5;

            if (addSmallChild)
            {
                foreach (PuzzlePieceObject smallObject in smallObjects)
                {
                    if (smallObject.puzzlePiece.shape == mediumObject.puzzlePiece.shape && rand.NextDouble() > 0.5)
                    {
                        mediumObject.AddChild(smallObject);
                    }
                }
            }

            foreach (PuzzlePieceObject largeObject in largeObjects)
            {
                if (mediumObject.puzzlePiece.shape == largeObject.puzzlePiece.shape && rand.NextDouble() > 0.5)
                {
                    mediumObject.SetParent(largeObject);
                }
            }
        }

        foreach (PuzzlePieceObject smallObject in smallObjects)
        {
            foreach (PuzzlePieceObject mediumObject in mediumObjects)
            {
                if (smallObject.puzzlePiece.shape == mediumObject.puzzlePiece.shape && rand.NextDouble() > 0.5)
                {
                    smallObject.SetParent(mediumObject);
                }
            }

            foreach (PuzzlePieceObject largeObject in largeObjects)
            {
                if (smallObject.puzzlePiece.shape == largeObject.puzzlePiece.shape && rand.NextDouble() > 0.5)
                {
                    smallObject.SetParent(largeObject);
                }
            }
        }
    }

    private Mesh GetMesh(Shape shape, ShapeSize size)
    {
        Mesh mesh = new Mesh();
        foreach (PuzzlePieceMesh puzzlePieceMesh in allMeshes.Where(puzzlePieceMesh => puzzlePieceMesh.shape == shape && puzzlePieceMesh.size == size))
        {
            mesh = puzzlePieceMesh.mesh;
            break;
        }
        return mesh;
    }
    
    public Material[] GetMaterial(ShapeColor shapeColor, int materialCount)
    {
        List<Material> materials = new List<Material>();
        foreach (PieceMaterial pieceMaterial in pieceMaterials.Where(pieceMaterial => pieceMaterial.shapeColor == shapeColor))
        {
            for (int i = 0; i < materialCount; i++)
            {
                materials.Add(pieceMaterial.materials[i]);
            }
            break;
        }
        return materials.ToArray();
    }
    
    public void SlotMatch(Slot slot)
    {
        foreach (SlotPiece slotPiece in slot.slotPieces)
        {
            foreach (SlotPieceObject slotPieceObject in slotPieceObjects)
            {
                if(slotPieceObject.slotPiece.puzzlePiece.shape == slotPiece.puzzlePiece.shape && slotPieceObject.slotPiece.puzzlePiece.size == slotPiece.puzzlePiece.size)
                    slotPieceObject.Reset();
            }
        }
        CoreGamePlay.Singleton.ResetSlotContainMatch(slot);
        //Level Win
        if (puzzlePieceObjects.Count == 0) LevelGenerate();
    }

    public Transform GetSlotGroup(Shape shape)
    {
        foreach (var slotGroup in slotGroups.Where(slotGroup => slotGroup.shape == shape))
        {
            return slotGroup.sloTransform;
        }
        return null;
    }
    
    private void SetCameraSize()
    {
        float standardOrthographicSize = 9.5f;
        float standardCanvasWidth = 1080f;
        float standardCameraYPosition = 4.5f;
        float alternateStandardCameraYPosition = 2.1f;
        float alternateStandardOrthographicSize = 7.05f;
        float areaAdjustmentFactor = (standardCameraYPosition - alternateStandardCameraYPosition) /
                                     (standardOrthographicSize - alternateStandardOrthographicSize);
        
        mainCamera.orthographicSize = standardOrthographicSize * Mathf.Pow(standardCanvasWidth / canvasRectTransform.sizeDelta.x, 2);
        float newCameraYPosition = standardCameraYPosition - (standardOrthographicSize - mainCamera.orthographicSize) * areaAdjustmentFactor;
        mainCamera.transform.position = new Vector3(mainCamera.transform.position.x, newCameraYPosition, mainCamera.transform.position.z);
    }
}
