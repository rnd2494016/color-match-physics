using System;
using System.Collections.Generic;
using FrustumCullingSpace;
using UnityEngine;

public enum  Shape
{
    Triangle,
    Circle,
    Rectangle,
    Pentagon
}

public enum  ShapeSize
{
    Large,
    Medium,
    Small
}

public enum ShapeColor
{
    NoColor,
    Red,
    Blue,
    Green,
    Orange,
    Pink
}

[Serializable]
public struct PuzzlePiece
{
    public Shape shape;
    public ShapeSize size;
    public ShapeColor color;
    public int materialCount;
}
[Serializable]
public class SlotPiece
{
    public PuzzlePiece puzzlePiece;
    public bool isEmpty = true;
}

[Serializable]
public class Slot
{
    public List<SlotPiece> slotPieces;
    public ShapeColor availableColor;
    public bool hasMatch;
}
[Serializable]
public class SlotGroup
{
    public Shape shape;
    public Transform sloTransform;
}

[Serializable]   
public struct LevelInfo
{
    public int levelNumber;
    public List<PuzzlePiece> puzzlePieces;
    public List<Slot> slots;
    //public Goal
}

[Serializable]
public struct PuzzlePieceMesh
{
    public Shape shape;
    public ShapeSize size;
    public Mesh mesh;
}
[Serializable]
public struct PieceMaterial
{
    public ShapeColor shapeColor;
    public List<Material> materials;
}

#region Data Serializers
[Serializable]
public struct PuzzlePieceData
{
    public string color;
    public int set;
}

[Serializable]
public struct SinglePieceData
{
    public string shape;
    public List<PuzzlePieceData> puzzle_piece_data;
}

[Serializable]
public struct LevelData
{
    public int level_number;
    public List<SinglePieceData> puzzle_piece;
    public List<SinglePieceData> slot_piece;
}
[Serializable]
public struct AllLevelData
{
    public List<LevelData> levels;
}

#endregion


public static class Extensions
{
    public static void AddCollider(this GameObject gameObject, Shape colliderType)
    {
        switch (colliderType)
        {
            case Shape.Triangle:
                if (gameObject.GetComponent<MeshCollider>() == null)
                {
                    MeshCollider collider = gameObject.AddComponent<MeshCollider>();
                    collider.convex = true;
                }
                break;
            case Shape.Circle:
                if (gameObject.GetComponent<SphereCollider>() == null)
                {
                    gameObject.AddComponent<SphereCollider>();
                }
                break;
            case Shape.Rectangle:
                if (gameObject.GetComponent<BoxCollider>() == null)
                {
                    gameObject.AddComponent<BoxCollider>();
                }
                break;
            case Shape.Pentagon:
                if (gameObject.GetComponent<MeshCollider>() == null)
                {
                    MeshCollider collider = gameObject.AddComponent<MeshCollider>();
                    collider.convex = true;
                }
                break;
            default:
                Debug.LogWarning("Collider type not recognized.");
                break;
        }
    }

    public static void AddFrustumCulling(this GameObject gameObject)
    {
        FrustumCullingObject frustumCullingObject = gameObject.AddComponent<FrustumCullingObject>();
        frustumCullingObject.BuildEdges();
    }
}